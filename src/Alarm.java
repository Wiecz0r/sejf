import java.util.ArrayList;
import java.util.List;


public class Alarm {
	private List<AlarmListener> listeners;
	private String pin = "0000";
	
	private Thread line = new Thread(){
		
		public void run(){
			
			while(true){
				
				if(pin.equals(getPin())){
					alarmTurnedOff();
				}
				else{
					alarmTurnedOn();
				}
				
				try {	
					sleep(3000);
					
				}catch (InterruptedException event){
					event.printStackTrace();
				}
				
			}
		}
	};
	
	public String getPin() {
		return "0000" ;
	}
	public Alarm(String pin){
		this.pin=pin;
		this.listeners=new ArrayList<AlarmListener>();
		line.start();
	}
	public synchronized void addListener(AlarmListener listener){
		listeners.add(listener);
	}
	public synchronized void removeListener(AlarmListener listener){
		listeners.remove(listener);
	}
	public synchronized void alarmTurnedOff(){
		System.out.println("alarm off");
		EnteredPinEvent event = new EnteredPinEvent(this);
		for(AlarmListener listener: listeners){
			listener.alarmTurnedOff(event);
		}
	}
	public synchronized void alarmTurnedOn(){
		System.out.println("alarm on");
		EnteredPinEvent event = new EnteredPinEvent(this);
		for(AlarmListener listener: listeners){
			listener.alarmTurnedOn(event);
		}
	}
}
