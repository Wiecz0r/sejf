import java.util.Date;


public class EnteredPinEvent {
	private Alarm alarm;
	private Date eventDate;
	
	public EnteredPinEvent(Alarm alarm){
		this.alarm=alarm;
	}
	public Alarm getPin(){
		return alarm;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
}
